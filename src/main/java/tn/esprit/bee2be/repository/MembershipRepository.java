package tn.esprit.bee2be.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.esprit.bee2be.entity.model.Membership;

public interface MembershipRepository  extends JpaRepository<Membership, Long> {
}
