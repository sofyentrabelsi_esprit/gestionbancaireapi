package tn.esprit.bee2be.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tn.esprit.bee2be.entity.model.Account;
import tn.esprit.bee2be.entity.model.Beneficiary;

import java.util.List;

@Repository
public interface BeneficiaryRepository extends JpaRepository<Beneficiary, Long> {

    @Query("SELECT b FROM Beneficiary b")
    public List<Beneficiary> getAllBeneficiaryJPQL();
}
