
package tn.esprit.bee2be.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.esprit.bee2be.entity.model.Products;

@Repository
public interface ProductsRepository extends JpaRepository<Products, Long>{

}
