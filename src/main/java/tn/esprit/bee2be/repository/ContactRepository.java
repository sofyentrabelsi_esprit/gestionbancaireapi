package tn.esprit.bee2be.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.esprit.bee2be.entity.model.Contact;

public interface ContactRepository extends JpaRepository<Contact, Long> {
}
