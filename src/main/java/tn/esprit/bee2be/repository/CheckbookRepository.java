package tn.esprit.bee2be.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.esprit.bee2be.entity.model.Checkbook;

@Repository
public interface CheckbookRepository extends JpaRepository<Checkbook, Long>{

}
