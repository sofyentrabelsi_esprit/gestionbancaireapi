package tn.esprit.bee2be.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tn.esprit.bee2be.entity.model.Loan;
@Repository
public interface LoanRepository extends JpaRepository<Loan, Long>{

}
