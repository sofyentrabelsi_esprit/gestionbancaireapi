package tn.esprit.bee2be.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.esprit.bee2be.entity.model.ContactType;

public interface ContactTypeRepository  extends JpaRepository<ContactType, Long> {
}
