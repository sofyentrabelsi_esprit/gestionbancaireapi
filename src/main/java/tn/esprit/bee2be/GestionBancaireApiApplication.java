package tn.esprit.bee2be;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import tn.esprit.bee2be.configuration.FileStorageProperties;

@SpringBootApplication
@EnableConfigurationProperties({ FileStorageProperties.class })
public class GestionBancaireApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionBancaireApiApplication.class, args);
	}

}
