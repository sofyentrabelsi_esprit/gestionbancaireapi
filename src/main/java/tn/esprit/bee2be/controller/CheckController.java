package tn.esprit.bee2be.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tn.esprit.bee2be.entity.model.Check;
import tn.esprit.bee2be.service.IService.ICheckService;


@RestController
public class CheckController {

	@Autowired
	ICheckService checkService;
	@PostMapping("/addCheck")
	@ResponseBody
	public Check addCheck(HttpServletRequest httpServletRequest, @RequestBody Check check) { 
		return checkService.add(check);
	}
	@GetMapping("/getCheck/{id}")
	@ResponseBody
	public Check getCheck(@PathVariable("id") long id) {
		return checkService.get(id);
		 
	}
	@GetMapping("/getAllCheck")
	@ResponseBody
	public List<Check> getAllCheck() {
		return checkService.getAll();
		
	}
	@DeleteMapping("/deleteAllCheck")
	@ResponseBody
	public void deleteAllCheck() {
		checkService.deleteAll();
	}
	@DeleteMapping("/deleteCheck/{id}")
	@ResponseBody
	public void deleteAccount(@PathVariable("id") long id) {
		Check check=checkService.get(id);
		checkService.delete(check);
	}
	//http://localhost:8880/bee2be/servlet/deleteCheck/1

}
