package tn.esprit.bee2be.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import tn.esprit.bee2be.entity.model.Account;
import tn.esprit.bee2be.service.IService.IAccountService;

@RestController
public class AccountController {
	@Autowired
	IAccountService accountService;
	
	@PostMapping("/addAccount")
	@ResponseBody
	public Account addAccount(HttpServletRequest httpServletRequest, @RequestBody Account account) { 
		return accountService.add(account);
	}
	@GetMapping("/getAccount/{id}")
	@ResponseBody
	public Account getAccount(@PathVariable("id") long id) {
		return accountService.get(id);
		 
	}
	@GetMapping("/getAllAccount")
	@ResponseBody
	public List<Account> getAllAccount() {
		return accountService.getAll();
		
	}

	@DeleteMapping("/deleteAllAccount")
	@ResponseBody
	public void deleteAllAccount() {
		 accountService.deleteAll();
		
	}
	
	@DeleteMapping("/deleteAccount/{id}")
	@ResponseBody
	public void deleteAccount(@PathVariable("id") long id) {
		Account account=accountService.get(id);
		accountService.delete(account);
		
	}
	 @PutMapping("/affecterAccountAProduct/{accountId}/{productId}")
	 @ResponseBody
	 public void affecterAccountAProduct(@PathVariable long accountId, @PathVariable long productId){
		 accountService.affecterAccountAProduct(accountId, productId);
	 }
}
