package tn.esprit.bee2be.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tn.esprit.bee2be.entity.model.Card;
import tn.esprit.bee2be.service.IService.ICardService;
import tn.esprit.bee2be.service.IService.ICheckbookService;

@RestController
public class CardController {
	@Autowired
	ICardService cardService;

	@PostMapping("/addCard")
	@ResponseBody
	public Card addCard(HttpServletRequest httpServletRequest, @RequestBody Card card) { 
		return cardService.add(card);
	}
	@GetMapping("/getCard/{id}")
	@ResponseBody
	public Card getCheckbook(@PathVariable("id") long id) {
		return cardService.get(id);
		 
	}
	@GetMapping("/getAllCard")
	@ResponseBody
	public List<Card> getAllCard() {
		return cardService.getAll();
		
	}
	@DeleteMapping("/deleteAllCard")
	@ResponseBody
	public void deleteAllCard() {
		cardService.deleteAll();
	}
	@DeleteMapping("/deleteCard/{id}")
	@ResponseBody
	public void deleteCard(@PathVariable("id") long id) {
		Card card=cardService.get(id);
		cardService.delete(card);
	}
	//http://localhost:8082/bee2be/servlet/affecterCardAProduct/5/1
	 @PutMapping("/affecterCardAProduct/{cardId}/{productId}")
	 @ResponseBody
	 public void affecterCardAProduct(@PathVariable long cardId, @PathVariable long productId){
		 cardService.affecterCardAProduct(cardId, productId);
	 }
}
