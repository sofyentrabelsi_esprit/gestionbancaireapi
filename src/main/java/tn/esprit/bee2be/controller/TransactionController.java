package tn.esprit.bee2be.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tn.esprit.bee2be.entity.model.Transaction;
import tn.esprit.bee2be.service.IService.IAccountService;
import tn.esprit.bee2be.service.IService.ITransactionService;

@RestController
public class TransactionController {
	@Autowired
	ITransactionService transactionService;
	@Autowired
	IAccountService accountService;
	
	
	
	@PostMapping("/addTransaction")
	@ResponseBody
	public Transaction addTransaction(HttpServletRequest httpServletRequest, @RequestBody Transaction transaction) { 
		return transactionService.add(transaction);
	}
	@GetMapping("/getTransaction/{id}")
	@ResponseBody
	public Transaction getTransaction(@PathVariable("id") long id) {
		return transactionService.get(id);
		 
	}
	@GetMapping("/getAllTransaction")
	@ResponseBody
	public List<Transaction> getAllTransaction() {
		return transactionService.getAll();
		
	}
	@DeleteMapping("/deleteAllTransaction")
	@ResponseBody
	public void deleteAllTransaction() {
		transactionService.deleteAll();
	}
	@DeleteMapping("/deleteTransaction/{id}")
	@ResponseBody
	public void deleteTransaction(@PathVariable("id") long id) {
		Transaction transaction=transactionService.get(id);
		transactionService.delete(transaction);
	}
	//http://localhost:8082/bee2be/servlet/affecterTransactionAAccount/5/1
	 @PutMapping("/affecterTransactionnAAccount/{transactionId}/{accountId}")
	 @ResponseBody
	 public void affecterTransactionnAAccount(@PathVariable long transactionId, @PathVariable long accountId){
		 transactionService.affecterTransactionnAAccount(transactionId, accountId);
	 }
}
