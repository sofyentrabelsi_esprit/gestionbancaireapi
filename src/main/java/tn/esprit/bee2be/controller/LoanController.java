package tn.esprit.bee2be.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tn.esprit.bee2be.entity.model.Loan;
import tn.esprit.bee2be.service.IService.ILoanService;

@RestController
public class LoanController {
	@Autowired
	ILoanService loanService;
	@PostMapping("/addLoan")
	@ResponseBody
	public Loan addLoan(HttpServletRequest httpServletRequest, @RequestBody Loan loan) { 
		return loanService.add(loan);
	}
	@GetMapping("/getLoan/{id}")
	@ResponseBody
	public Loan getLoan(@PathVariable("id") long id) {
		return loanService.get(id);
		 
	}
	@GetMapping("/getAllLoan")
	@ResponseBody
	public List<Loan> getAllLoan() {
		return loanService.getAll();
		
	}
	@DeleteMapping("/deleteAllLoan")
	@ResponseBody
	public void deleteAllLoan() {
		loanService.deleteAll();
	}
	@DeleteMapping("/deleteLoan/{id}")
	@ResponseBody
	public void deleteLoan(@PathVariable("id") long id) {
		Loan loan=loanService.get(id);
		loanService.delete(loan);
	}
	//http://localhost:8082/bee2be/servlet/affecterLoanAProduct/5/1
	 @PutMapping("/affecterLoanAProduct/{loanId}/{productId}")
	 @ResponseBody
	 public void affecterLoanAProduct(@PathVariable long loanId, @PathVariable long productId){
		 loanService.affecterLoanAProduct(loanId, productId);
	 }
}
