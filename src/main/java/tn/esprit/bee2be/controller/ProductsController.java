package tn.esprit.bee2be.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tn.esprit.bee2be.entity.model.Products;
import tn.esprit.bee2be.service.IService.IProductsService;

@RestController
public class ProductsController {
	@Autowired
	IProductsService productsService;
	@PostMapping("/addProduct")
	@ResponseBody
	public Products addTransaction(HttpServletRequest httpServletRequest, @RequestBody Products product) { 
		return productsService.add(product);
	}
	@GetMapping("/getProduct/{id}")
	@ResponseBody
	public Products getTransaction(@PathVariable("id") long id) {
		return productsService.get(id);
		 
	}
	@GetMapping("/getAllProducts")
	@ResponseBody
	public List<Products> getAllProducts() {
		return productsService.getAll();
		
	}
	@DeleteMapping("/deleteAllProducts")
	@ResponseBody
	public void deleteAllProducts() {
		productsService.deleteAll();
	}
	@DeleteMapping("/deleteProduct/{id}")
	@ResponseBody
	public void deleteProduct(@PathVariable("id") long id) {
		Products product=productsService.get(id);
		productsService.delete(product);
	}
}
