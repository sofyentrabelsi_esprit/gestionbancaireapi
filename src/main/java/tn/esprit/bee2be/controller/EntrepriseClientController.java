package tn.esprit.bee2be.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tn.esprit.bee2be.entity.model.EntrepriseClient;
import tn.esprit.bee2be.service.IService.IEntrepriseClientService;

@RestController
public class EntrepriseClientController {

	
	@Autowired
	IEntrepriseClientService entrepriseClientService;
	@PostMapping("/addEntrepriseClient")
	@ResponseBody
	public EntrepriseClient addEntrepriseClient(HttpServletRequest httpServletRequest, @RequestBody EntrepriseClient entrepriseClient) { 
		return entrepriseClientService.add(entrepriseClient);
	}
	@GetMapping("/getEntrepriseClient/{id}")
	@ResponseBody
	public EntrepriseClient getEntrepriseClient(@PathVariable("id") long id) {
		return entrepriseClientService.get(id);
		 
	}
	@GetMapping("/getAllEntrepriseClient")
	@ResponseBody
	public List<EntrepriseClient> getAllEntrepriseClient() {
		return entrepriseClientService.getAll();
		
	}
	@DeleteMapping("/deleteAllEntrepriseClient")
	@ResponseBody
	public void deleteAllEntrepriseClient() {
		entrepriseClientService.deleteAll();
	}
	@DeleteMapping("/deleteEntrepriseClient/{id}")
	@ResponseBody
	public void deleteAccount(@PathVariable("id") long id) {
		EntrepriseClient entrepriseClient=entrepriseClientService.get(id);
		entrepriseClientService.delete(entrepriseClient);
	}
	//http://localhost:8880/bee2be/servlet/deleteEntrepriseClient/1
}
