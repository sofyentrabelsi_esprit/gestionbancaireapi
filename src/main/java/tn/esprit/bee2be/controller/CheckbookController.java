package tn.esprit.bee2be.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import tn.esprit.bee2be.entity.model.Checkbook;
import tn.esprit.bee2be.service.IService.ICheckbookService;

@RestController
public class CheckbookController {
	@Autowired
	ICheckbookService checkbookService;
	@PostMapping("/addCheckbook")
	@ResponseBody
	public Checkbook addCheckbook(HttpServletRequest httpServletRequest, @RequestBody Checkbook checkbook) { 
		return checkbookService.add(checkbook);
	}
	@GetMapping("/getCheckbook/{id}")
	@ResponseBody
	public Checkbook getCheckbook(@PathVariable("id") long id) {
		return checkbookService.get(id);
		 
	}
	@GetMapping("/getAllCheckbook")
	@ResponseBody
	public List<Checkbook> getAllCheckbook() {
		return checkbookService.getAll();
		
	}
	@DeleteMapping("/deleteAllCheckbook")
	@ResponseBody
	public void deleteAllCheckbook() {
		checkbookService.deleteAll();
	}
	@DeleteMapping("/deleteCheckbook/{id}")
	@ResponseBody
	public void deleteAccount(@PathVariable("id") long id) {
		Checkbook checkbook=checkbookService.get(id);
		checkbookService.delete(checkbook);
	}
	//http://localhost:8082/bee2be/servlet/affecterCheckbookAProduct/5/1
	 @PutMapping("/affecterCheckbookAProduct/{checkbookId}/{productId}")
	 @ResponseBody
	 public void affecterCheckbookAProduct(@PathVariable long checkbookId, @PathVariable long productId){
		 checkbookService.affecterCheckbookAProduct(checkbookId, productId);
	 }
}
