package tn.esprit.bee2be.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tn.esprit.bee2be.entity.model.Banque;
import tn.esprit.bee2be.service.IService.IBanqueService;

@RestController
public class BanqueController {

	
	@Autowired
	IBanqueService banqueService;
	@PostMapping("/addBanque")
	@ResponseBody
	public Banque addBanque(HttpServletRequest httpServletRequest, @RequestBody Banque banque) { 
		return banqueService.add(banque);
	}
	@GetMapping("/getBanque/{id}")
	@ResponseBody
	public Banque getBanque(@PathVariable("id") long id) {
		return banqueService.get(id);
		 
	}
	@GetMapping("/getAllBanque")
	@ResponseBody
	public List<Banque> getAllBanque() {
		return banqueService.getAll();
		
	}
	@DeleteMapping("/deleteAllBanque")
	@ResponseBody
	public void deleteAllBanque() {
		banqueService.deleteAll();
	}
	@DeleteMapping("/deleteBanque/{id}")
	@ResponseBody
	public void deleteAccount(@PathVariable("id") long id) {
		Banque banque=banqueService.get(id);
		banqueService.delete(banque);
	}
	//http://localhost:8880/bee2be/servlet/deleteBanque/1
}
