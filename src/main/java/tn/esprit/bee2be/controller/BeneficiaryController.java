package tn.esprit.bee2be.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tn.esprit.bee2be.entity.model.Beneficiary;
import tn.esprit.bee2be.service.IService.IBeneficiaryService;

import javax.servlet.http.HttpServletRequest;

@RestController
public class BeneficiaryController {

    @Autowired
    IBeneficiaryService beneficiaryService;

    @PostMapping("/addBeneficiary")
    @ResponseBody
    public Beneficiary addBeneficiary(HttpServletRequest httpServletRequest, @RequestBody Beneficiary beneficiary) {
        return beneficiaryService.add(beneficiary);
    }


    @GetMapping("/getBeneficiary/{id}")
    @ResponseBody
    public Beneficiary getBeneficiary(@PathVariable("id") long id) {
        return beneficiaryService.get(id);
    }
}
