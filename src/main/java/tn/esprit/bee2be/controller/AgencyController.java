package tn.esprit.bee2be.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tn.esprit.bee2be.entity.model.Agency;
import tn.esprit.bee2be.service.IService.IAgencyService;

@RestController
public class AgencyController {

	@Autowired
	IAgencyService agencyService;
	@PostMapping("/addAgency")
	@ResponseBody
	public Agency addAgency(HttpServletRequest httpServletRequest, @RequestBody Agency agency) { 
		return agencyService.add(agency);
	}
	@GetMapping("/getAgency/{id}")
	@ResponseBody
	public Agency getAgency(@PathVariable("id") long id) {
		return agencyService.get(id);
		 
	}
	@GetMapping("/getAllAgency")
	@ResponseBody
	public List<Agency> getAllAgency() {
		return agencyService.getAll();
		
	}
	@DeleteMapping("/deleteAllAgency")
	@ResponseBody
	public void deleteAllAgency() {
		agencyService.deleteAll();
	}
	@DeleteMapping("/deleteAgency/{id}")
	@ResponseBody
	public void deleteAccount(@PathVariable("id") long id) {
		Agency agency=agencyService.get(id);
		agencyService.delete(agency);
	}
	//http://localhost:8880/bee2be/servlet/deleteAgency/1
}
