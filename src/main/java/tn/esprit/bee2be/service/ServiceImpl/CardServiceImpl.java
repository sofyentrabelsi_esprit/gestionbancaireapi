package tn.esprit.bee2be.service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.bee2be.entity.model.Card;
import tn.esprit.bee2be.entity.model.Checkbook;
import tn.esprit.bee2be.entity.model.Products;
import tn.esprit.bee2be.repository.CardRepository;
import tn.esprit.bee2be.repository.ProductsRepository;
import tn.esprit.bee2be.service.IService.ICardService;
@Service
public class CardServiceImpl implements ICardService{
	   @Autowired
	   CardRepository cardRepository;
	   @Autowired
	  ProductsRepository productsRepository;
	@Override
	public Card add(Card card) {
		return cardRepository.save(card);
	}

	@Override
	public Card update(Card card) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Card get(Long id) {
		return cardRepository.findById(id).get();
	}

	@Override
	public void delete(Card card) {
		cardRepository.delete(card);		
	}

	@Override
	public void deleteAll() {
		cardRepository.deleteAll();		
	}

	@Override
	public List<Card> getAll() {
		List<Card> cards= cardRepository.findAll();
		return cards;
	}
	@Override
	public void affecterCardAProduct(long cardId, long productId) {
		Card card=cardRepository.findById(cardId).get();
		Products products=productsRepository.findById(productId).get();
		card.setProducts(products);
		cardRepository.save(card);
	}

	
}
