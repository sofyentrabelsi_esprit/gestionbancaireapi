package tn.esprit.bee2be.service.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.bee2be.entity.model.Contact;
import tn.esprit.bee2be.repository.ContactRepository;
import tn.esprit.bee2be.service.IService.IContactService;

import java.util.List;

@Service
public class ContactServiceImpl implements IContactService {
    @Autowired
    ContactRepository contactRepository;
    @Override
    public Contact add(Contact entity) {
        return null;
    }

    @Override
    public Contact update(Contact entity) {
        return null;
    }

    @Override
    public Contact get(Long id) {
        return null;
    }

    @Override
    public void delete(Contact entity) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public List<Contact> getAll() {
        return null;
    }
}
