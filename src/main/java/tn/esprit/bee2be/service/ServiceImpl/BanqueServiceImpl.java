package tn.esprit.bee2be.service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.bee2be.entity.model.Banque;
import tn.esprit.bee2be.repository.BanqueRepository;
import tn.esprit.bee2be.service.IService.IBanqueService;

@Service
public class BanqueServiceImpl implements IBanqueService{

	@Autowired
	BanqueRepository banqueRepository;
	
	@Override
	public Banque add(Banque banque) {
		return banqueRepository.save(banque);
	}

	@Override
	public Banque update(Banque banque) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Banque get(Long id) {
		return banqueRepository.findById(id).get();
		
	}

	@Override
	public void delete(Banque banque) {
		banqueRepository.delete(banque);
	}

	@Override
	public void deleteAll() {
		banqueRepository.deleteAll();		
	}

	@Override
	public List<Banque> getAll() {
		List<Banque> banques=banqueRepository.findAll();
		return banques;
	}
	
}
