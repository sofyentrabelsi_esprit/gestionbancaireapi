package tn.esprit.bee2be.service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.bee2be.entity.model.Account;
import tn.esprit.bee2be.entity.model.Transaction;
import tn.esprit.bee2be.repository.AccountRepository;
import tn.esprit.bee2be.repository.TransactionRepository;
import tn.esprit.bee2be.service.IService.ITransactionService;
@Service
public class TransactionServiceImpl implements ITransactionService  {
   @Autowired
   TransactionRepository transactionRepository;
   @Autowired
AccountRepository accountRepository; 
	@Override
	public Transaction add(Transaction transaction) {
		return transactionRepository.save(transaction);
	}

	@Override
	public Transaction update(Transaction transaction) {
		return null;
	}

	@Override
	public Transaction get(Long id) {
		return transactionRepository.findById(id).get();		
	}

	@Override
	public void delete(Transaction transaction) {
		transactionRepository.delete(transaction);		
	}

	@Override
	public void deleteAll() {
		transactionRepository.deleteAll();		
	}

	@Override
	public List<Transaction> getAll() {
		return transactionRepository.findAll();
	}
	@Override
	public void affecterTransactionnAAccount(long transactionId, long accountId) {
		Transaction transaction=transactionRepository.findById(transactionId).get();
		Account account=accountRepository.findById(accountId).get();
		transaction.setAccount(account);
		transactionRepository.save(transaction);
	}
}
