package tn.esprit.bee2be.service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.bee2be.entity.model.Products;
import tn.esprit.bee2be.repository.ProductsRepository;
import tn.esprit.bee2be.service.IService.IProductsService;

@Service
public class ProductsServiceImpl implements IProductsService {
    @Autowired
    ProductsRepository productsRepository;
	@Override
	public Products add(Products product) {
		return productsRepository.save(product);
	}

	@Override
	public Products update(Products product) {
		return null;
	}

	@Override
	public Products get(Long id) {
		return productsRepository.findById(id).get();
		 
	}

	@Override
	public void delete(Products product) {
		productsRepository.delete(product);		
	}

	@Override
	public void deleteAll() {
		productsRepository.deleteAll();		
	}

	@Override
	public List<Products> getAll() {
		List<Products> products=productsRepository.findAll();
		return products;
	}

}
