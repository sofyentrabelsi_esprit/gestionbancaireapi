package tn.esprit.bee2be.service.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.bee2be.entity.model.Beneficiary;
import tn.esprit.bee2be.repository.BeneficiaryRepository;
import tn.esprit.bee2be.service.IService.IBeneficiaryService;

import java.util.Date;
import java.util.List;

@Service
public class BeneficiaryServiceImpl implements IBeneficiaryService {

    @Autowired
    BeneficiaryRepository beneficiaryRepository;

    @Override
    public void addBeneficiary(String firstName, String lastName, String rib, String adress, Date createdAt, int currentUserId) {
        // todo : add currentUserId To Affect User to account
        beneficiaryRepository.save(new Beneficiary(firstName, lastName, rib, adress, createdAt));
    }

    @Override
    public Beneficiary add(Beneficiary entity) {
        beneficiaryRepository.save(entity);
        return entity;
    }

    @Override
    public Beneficiary update(Beneficiary entity) {
        return beneficiaryRepository.save(entity);
    }

    @Override
    public Beneficiary get(Long id) {
        Beneficiary beneficiary = beneficiaryRepository.findById(id).get();
        return beneficiary;
    }

    @Override
    public void delete(Beneficiary entity) {
        beneficiaryRepository.delete(entity);
    }

    @Override
    public void deleteAll() {
        beneficiaryRepository.deleteAll();
    }

    @Override
    public List<Beneficiary> getAll() {
        return beneficiaryRepository.getAllBeneficiaryJPQL();
    }
}
