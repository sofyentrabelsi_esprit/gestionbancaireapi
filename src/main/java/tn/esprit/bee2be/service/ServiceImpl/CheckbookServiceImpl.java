package tn.esprit.bee2be.service.ServiceImpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.bee2be.entity.model.Checkbook;
import tn.esprit.bee2be.entity.model.Products;
import tn.esprit.bee2be.repository.CheckbookRepository;
import tn.esprit.bee2be.repository.ProductsRepository;
import tn.esprit.bee2be.service.IService.ICheckbookService;

@Service
public class CheckbookServiceImpl implements ICheckbookService{
	@Autowired
	CheckbookRepository checkbookRepository;
	@Autowired
	ProductsRepository  productsRepository;
	@Override
	public Checkbook add(Checkbook checkbook) {
		return checkbookRepository.save(checkbook);
	}

	@Override
	public Checkbook update(Checkbook checkbook) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Checkbook get(Long id) {
		return checkbookRepository.findById(id).get();
		
	}

	@Override
	public void delete(Checkbook checkbook) {
		checkbookRepository.delete(checkbook);
	}

	@Override
	public void deleteAll() {
		checkbookRepository.deleteAll();		
	}

	@Override
	public List<Checkbook> getAll() {
		List<Checkbook> checkbooks=checkbookRepository.findAll();
		return checkbooks;
	}
	@Override
	public void affecterCheckbookAProduct(long checkbookId, long productId) {
		Checkbook checkbook=checkbookRepository.findById(checkbookId).get();
		Products products=productsRepository.findById(productId).get();
		checkbook.setProducts(products);
		checkbookRepository.save(checkbook);
	}
	
}
