package tn.esprit.bee2be.service.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.bee2be.entity.model.ContactType;
import tn.esprit.bee2be.repository.ContactTypeRepository;
import tn.esprit.bee2be.service.IService.IContactTypeService;

import java.util.List;

@Service
public class ContactTypeServiceImpl implements IContactTypeService {
    @Autowired
    ContactTypeRepository contactTypeRepository;

    @Override
    public ContactType add(ContactType entity) {
        return null;
    }

    @Override
    public ContactType update(ContactType entity) {
        return null;
    }

    @Override
    public ContactType get(Long id) {
        return null;
    }

    @Override
    public void delete(ContactType entity) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public List<ContactType> getAll() {
        return null;
    }
}
