package tn.esprit.bee2be.service.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.esprit.bee2be.entity.model.Membership;
import tn.esprit.bee2be.repository.MembershipRepository;
import tn.esprit.bee2be.service.IService.IMembershipService;

import java.util.List;

@Service
public class MembershipServiceImpl implements IMembershipService {

    @Autowired
    MembershipRepository membershipRepository;

    @Override
    public Membership add(Membership entity) {
        return null;
    }

    @Override
    public Membership update(Membership entity) {
        return null;
    }

    @Override
    public Membership get(Long id) {
        return null;
    }

    @Override
    public void delete(Membership entity) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public List<Membership> getAll() {
        return null;
    }
}
