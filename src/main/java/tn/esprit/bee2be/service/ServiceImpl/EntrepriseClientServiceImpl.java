package tn.esprit.bee2be.service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.bee2be.entity.model.EntrepriseClient;
import tn.esprit.bee2be.repository.EntrepriseClientRepository;
import tn.esprit.bee2be.service.IService.IEntrepriseClientService;

@Service
public class EntrepriseClientServiceImpl implements IEntrepriseClientService{

	@Autowired
	EntrepriseClientRepository entrepriseClientRepository;
	
	@Override
	public EntrepriseClient add(EntrepriseClient entrepriseClient) {
		return entrepriseClientRepository.save(entrepriseClient);
	}

	@Override
	public EntrepriseClient update(EntrepriseClient entrepriseClient) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EntrepriseClient get(Long id) {
		return entrepriseClientRepository.findById(id).get();
		
	}

	@Override
	public void delete(EntrepriseClient entrepriseClient) {
		entrepriseClientRepository.delete(entrepriseClient);
	}

	@Override
	public void deleteAll() {
		entrepriseClientRepository.deleteAll();		
	}

	@Override
	public List<EntrepriseClient> getAll() {
		List<EntrepriseClient> entrepriseClients=entrepriseClientRepository.findAll();
		return entrepriseClients;
	}

	
}
