package tn.esprit.bee2be.service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.bee2be.entity.model.Agency;
import tn.esprit.bee2be.repository.AgencyRepository;
import tn.esprit.bee2be.service.IService.IAgencyService;

@Service
public class AgencyServiceImpl implements IAgencyService{

	@Autowired
	AgencyRepository agencyRepository;
	
	@Override
	public Agency add(Agency agency) {
		return agencyRepository.save(agency);
	}

	@Override
	public Agency update(Agency agency) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Agency get(Long id) {
		return agencyRepository.findById(id).get();
		
	}

	@Override
	public void delete(Agency agency) {
		agencyRepository.delete(agency);
	}

	@Override
	public void deleteAll() {
		agencyRepository.deleteAll();		
	}

	@Override
	public List<Agency> getAll() {
		List<Agency> agencys=agencyRepository.findAll();
		return agencys;
	}
	
}
