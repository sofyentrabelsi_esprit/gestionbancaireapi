package tn.esprit.bee2be.service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.bee2be.entity.model.Checkbook;
import tn.esprit.bee2be.entity.model.Loan;
import tn.esprit.bee2be.entity.model.Products;
import tn.esprit.bee2be.repository.LoanRepository;
import tn.esprit.bee2be.repository.ProductsRepository;
import tn.esprit.bee2be.service.IService.ILoanService;

@Service
public class LoanServiceImpl implements ILoanService{
    @Autowired
   LoanRepository loanRepository;
    @Autowired
    ProductsRepository productsRepository;
	@Override
	public Loan add(Loan loan) {
		return loanRepository.save(loan);
	}

	@Override
	public Loan update(Loan entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Loan get(Long id) {
		return loanRepository.findById(id).get();		 
	}

	@Override
	public void delete(Loan loan) {
		 loanRepository.delete(loan);	
	}

	@Override
	public void deleteAll() {
		loanRepository.deleteAll();		
	}

	@Override
	public List<Loan> getAll() {
		 List<Loan> loans=loanRepository.findAll();
		 return loans;
	}

	@Override
	public void affecterLoanAProduct(long loanId, long productId) {
		Loan loan=loanRepository.findById(loanId).get();
		Products products=productsRepository.findById(productId).get();
		loan.setProducts(products);
		loanRepository.save(loan);
	}
}
