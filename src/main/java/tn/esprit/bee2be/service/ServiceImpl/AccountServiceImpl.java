package tn.esprit.bee2be.service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import tn.esprit.bee2be.entity.model.Account;
import tn.esprit.bee2be.entity.model.Loan;
import tn.esprit.bee2be.entity.model.Products;
import tn.esprit.bee2be.repository.AccountRepository;
import tn.esprit.bee2be.repository.ProductsRepository;
import tn.esprit.bee2be.service.IService.IAccountService;
@Service
public class AccountServiceImpl implements IAccountService{
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    ProductsRepository productsRepository;
	@Override
	public Account add(Account account) {
		return accountRepository.save(account);

	}

	@Override
	public Account update(Account entity) {
		return null;
	}

	@Override
	public Account get(Long id) {
		Account account=accountRepository.findById(id).get();
		return account ;
		
	}

	@Override
	public void delete(Account entity) {
		 accountRepository.delete(entity);
		
	}

	@Override
	public void deleteAll() {
		 accountRepository.deleteAll();	
	}

	@Override
	public List<Account> getAll() {
		 List<Account> accounts= accountRepository.getAllAccountJPQL();
		 return accounts;
	}

	
	@Override
	public void affecterAccountAProduct(long accountId, long productId) {
		Account account=accountRepository.findById(accountId).get();
		Products products=productsRepository.findById(productId).get();
		account.setProducts(products);
		accountRepository.save(account);
	}
}
