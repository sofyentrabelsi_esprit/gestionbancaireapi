package tn.esprit.bee2be.service.ServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.bee2be.entity.model.Check;
import tn.esprit.bee2be.repository.CheckRepository;
import tn.esprit.bee2be.service.IService.ICheckService;

@Service
public class CheckServiceImpl implements ICheckService{

	@Autowired
	CheckRepository checkRepository;
	
	@Override
	public Check add(Check check) {
		return checkRepository.save(check);
	}

	@Override
	public Check update(Check check) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Check get(Long id) {
		return checkRepository.findById(id).get();
		
	}

	@Override
	public void delete(Check check) {
		checkRepository.delete(check);
	}

	@Override
	public void deleteAll() {
		checkRepository.deleteAll();		
	}

	@Override
	public List<Check> getAll() {
		List<Check> checks=checkRepository.findAll();
		return checks;
	}
}
