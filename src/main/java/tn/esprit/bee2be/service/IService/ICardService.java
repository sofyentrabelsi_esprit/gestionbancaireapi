package tn.esprit.bee2be.service.IService;

import tn.esprit.bee2be.entity.model.Card;

public interface ICardService extends IService<Card> {

	void affecterCardAProduct(long cardId, long productId);

}
