package tn.esprit.bee2be.service.IService;

import tn.esprit.bee2be.entity.model.ContactType;

public interface IContactTypeService  extends IService<ContactType>{
}
