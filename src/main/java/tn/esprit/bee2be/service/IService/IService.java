package tn.esprit.bee2be.service.IService;

import java.util.List;

public interface IService<T> {
    T add(T entity);

    T update(T entity);

    T get(Long id);

    void delete(T entity);

    void deleteAll();

    List<T> getAll();

}
