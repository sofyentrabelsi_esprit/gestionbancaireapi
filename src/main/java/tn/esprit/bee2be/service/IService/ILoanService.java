package tn.esprit.bee2be.service.IService;

import tn.esprit.bee2be.entity.model.Loan;

public interface ILoanService extends IService<Loan>{

	void affecterLoanAProduct(long loanId, long productId);

}
