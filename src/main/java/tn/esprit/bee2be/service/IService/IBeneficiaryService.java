package tn.esprit.bee2be.service.IService;

import tn.esprit.bee2be.entity.model.Beneficiary;

import java.util.Date;

public interface IBeneficiaryService extends IService<Beneficiary> {

    void addBeneficiary(String firstName, String lastName, String rib, String adress, Date createdAt, int currentUserId);

}
