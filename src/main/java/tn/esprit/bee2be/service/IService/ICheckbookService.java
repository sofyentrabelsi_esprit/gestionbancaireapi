package tn.esprit.bee2be.service.IService;

import tn.esprit.bee2be.entity.model.Checkbook;

public interface ICheckbookService extends IService<Checkbook>{

	void affecterCheckbookAProduct(long checkbookId, long productId);

}
