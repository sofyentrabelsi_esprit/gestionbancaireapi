package tn.esprit.bee2be.service.IService;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


public interface IFileStorageService {
	public String storeFile(MultipartFile file) throws IOException;

	public Resource loadFileAsResource(String fileName);
}
