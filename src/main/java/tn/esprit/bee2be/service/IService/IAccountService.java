package tn.esprit.bee2be.service.IService;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import tn.esprit.bee2be.entity.model.Account;

public interface IAccountService extends IService<Account>{

	void affecterAccountAProduct(long accountId, long productId);
	
}
