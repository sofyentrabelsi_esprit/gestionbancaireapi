package tn.esprit.bee2be.service.IService;

import tn.esprit.bee2be.entity.model.Transaction;

public interface ITransactionService extends IService<Transaction>{
	void affecterTransactionnAAccount(long transactionId, long accountId);

}
