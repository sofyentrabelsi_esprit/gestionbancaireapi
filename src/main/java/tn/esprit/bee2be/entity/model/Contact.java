package tn.esprit.bee2be.entity.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Contact")
public class Contact implements Serializable {
    private static final long serialVersionUID = -6640420807894570005L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String contactName;

    @OneToOne
    private ContactType contactType;

    public Contact() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }
}
