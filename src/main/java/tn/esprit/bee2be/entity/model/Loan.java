package tn.esprit.bee2be.entity.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Table(name="Loan")
@Entity
public class Loan implements Serializable {
	private static final long serialVersionUID = 6191889143079517027L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private long numberLoan;
	private long amount;
	//Capital restant dû
	private long remainingCapital;
	//Nombre d'échéances
    private int termOfLoan;
    private long interestRate;
	@Temporal(TemporalType.DATE)
	private Date startonDate;
	@ManyToOne
	private Products products;
	
	
	public Products getProducts() {
		return products;
	}
	public void setProducts(Products products) {
		this.products = products;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public long getNumberLoan() {
		return numberLoan;
	}
	public void setNumberLoan(long numberLoan) {
		this.numberLoan = numberLoan;
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public long getRemainingCapital() {
		return remainingCapital;
	}
	public void setRemainingCapital(long remainingCapital) {
		this.remainingCapital = remainingCapital;
	}
	public int getTermOfLoan() {
		return termOfLoan;
	}
	public void setTermOfLoan(int termOfLoan) {
		this.termOfLoan = termOfLoan;
	}
	public long getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(long interestRate) {
		this.interestRate = interestRate;
	}
	public Date getStartonDate() {
		return startonDate;
	}
	public void setStartonDate(Date startonDate) {
		this.startonDate = startonDate;
	}
	public Loan(long id, long numberLoan, long amount, long remainingCapital, int termOfLoan, long interestRate,
			Date startonDate) {
		super();
		this.id = id;
		this.numberLoan = numberLoan;
		this.amount = amount;
		this.remainingCapital = remainingCapital;
		this.termOfLoan = termOfLoan;
		this.interestRate = interestRate;
		this.startonDate = startonDate;
	}
	public Loan() {
		
	}
	
}
