package tn.esprit.bee2be.entity.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import tn.esprit.bee2be.entity.enumeration.FeatureType;

@Entity
public class Feature implements Serializable {
	private static final long serialVersionUID = -5369734855993305723L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String name;
	
	private String logo;
	private String background;
	private FeatureType FeatureType;
	
	
	
	
	public Feature() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Feature(int id, String name, String logo, String background,
			tn.esprit.bee2be.entity.enumeration.FeatureType featureType) {
		super();
		this.id = id;
		this.name = name;
		this.logo = logo;
		this.background = background;
		FeatureType = featureType;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getBackground() {
		return background;
	}
	public void setBackground(String background) {
		this.background = background;
	}
	public FeatureType getFeatureType() {
		return FeatureType;
	}
	public void setFeatureType(FeatureType featureType) {
		FeatureType = featureType;
	}

	
	
	
	
}
