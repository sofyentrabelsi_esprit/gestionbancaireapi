package tn.esprit.bee2be.entity.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class ParticularClient extends PhysicalPerson {

    private String job;

    public ParticularClient() {
        super();
    }

    public ParticularClient(int id, String firstName, String lastName, String image, String cin, Date birthDate, String job) {
        super(id, firstName, lastName, image, cin, birthDate);
        this.job = job;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }
}
