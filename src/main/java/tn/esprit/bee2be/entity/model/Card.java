package tn.esprit.bee2be.entity.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="Card")
public class Card implements Serializable{
	private static final long serialVersionUID = 6191889143079517027L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String currency;
	private String cardHolderFullName;
	private int CCV;
	@Temporal(TemporalType.DATE)
	private Date  expirationDate;
	@ManyToOne
	private Products products;

	public Products getProducts() {
		return products;
	}
	public void setProducts(Products products) {
		this.products = products;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCardHolderFullName() {
		return cardHolderFullName;
	}
	public void setCardHolderFullName(String cardHolderFullName) {
		this.cardHolderFullName = cardHolderFullName;
	}
	public int getCCV() {
		return CCV;
	}
	public void setCCV(int cCV) {
		CCV = cCV;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	public Card(long id, String currency, String cardHolderFullName, int cCV, Date expirationDate) {
		super();
		this.id = id;
		this.currency = currency;
		this.cardHolderFullName = cardHolderFullName;
		CCV = cCV;
		this.expirationDate = expirationDate;
	}
	public Card() {
		super();
	}

}
