package tn.esprit.bee2be.entity.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ContactType")
public class ContactType implements Serializable {
    private static final long serialVersionUID = 5471487330685254519L;

    public ContactType(String name, String logo) {
        this.name = name;
        this.logo = logo;
    }

    public ContactType() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String logo;

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }


}
