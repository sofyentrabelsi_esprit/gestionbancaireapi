package tn.esprit.bee2be.entity.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Table(name="Checkbook")
@Entity
public class Checkbook implements Serializable{
	private static final long serialVersionUID = 6191889143079517027L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private boolean crossed;
	private int numberOfChecks;
	private int fees_per_transaction;
	@Temporal(TemporalType.DATE)
	private Date creationDate;
	@Temporal(TemporalType.DATE)
	private Date deleveryDate;
	
	@ManyToOne
	private Products products;
	

	public Products getProducts() {
		return products;
	}
	public void setProducts(Products products) {
		this.products = products;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public boolean isCrossed() {
		return crossed;
	}
	public void setCrossed(boolean crossed) {
		this.crossed = crossed;
	}
	public int getNumberOfChecks() {
		return numberOfChecks;
	}
	public void setNumberOfChecks(int numberOfChecks) {
		this.numberOfChecks = numberOfChecks;
	}
	public int getFees_per_transaction() {
		return fees_per_transaction;
	}
	public void setFees_per_transaction(int fees_per_transaction) {
		this.fees_per_transaction = fees_per_transaction;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public Date getDeleveryDate() {
		return deleveryDate;
	}
	public void setDeleveryDate(Date deleveryDate) {
		this.deleveryDate = deleveryDate;
	}
	public Checkbook(long id, boolean crossed, int numberOfChecks, int fees_per_transaction, Date creationDate,
			Date deleveryDate) {
		super();
		this.id = id;
		this.crossed = crossed;
		this.numberOfChecks = numberOfChecks;
		this.fees_per_transaction = fees_per_transaction;
		this.creationDate = creationDate;
		this.deleveryDate = deleveryDate;
	}
	public Checkbook() {
		super();
	}
	
}
