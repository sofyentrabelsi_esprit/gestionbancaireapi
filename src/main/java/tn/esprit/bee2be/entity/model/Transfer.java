package tn.esprit.bee2be.entity.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import tn.esprit.bee2be.entity.enumeration.TransferType;

@Entity
public class Transfer implements Serializable {
	private static final long serialVersionUID = -5369734855993305723L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String Number;
	private TransferType TransferType;
	private String background;
	private String ProductType;
	private String AuthorizationList;
	
	
	
	
	
	public Transfer() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Transfer(int id, String number, tn.esprit.bee2be.entity.enumeration.TransferType transferType,
			String background, String productType, String authorizationList) {
		super();
		this.id = id;
		Number = number;
		TransferType = transferType;
		this.background = background;
		ProductType = productType;
		AuthorizationList = authorizationList;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNumber() {
		return Number;
	}
	public void setNumber(String number) {
		Number = number;
	}
	public TransferType getTransferType() {
		return TransferType;
	}
	public void setTransferType(TransferType transferType) {
		TransferType = transferType;
	}
	public String getBackground() {
		return background;
	}
	public void setBackground(String background) {
		this.background = background;
	}
	
	

}
