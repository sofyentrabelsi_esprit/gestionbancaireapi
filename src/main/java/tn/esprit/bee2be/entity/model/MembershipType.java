package tn.esprit.bee2be.entity.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MembershipType")
public class MembershipType implements Serializable {
    private static final long serialVersionUID = -1410886655362395566L;

    public MembershipType(String nom, String logl, double fees) {
        this.nom = nom;
        this.logl = logl;
        this.fees = fees;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nom;
    private String logl;
    private double fees;

    public MembershipType() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLogl() {
        return logl;
    }

    public void setLogl(String logl) {
        this.logl = logl;
    }

    public double getFees() {
        return fees;
    }

    public void setFees(double fees) {
        this.fees = fees;
    }
}
