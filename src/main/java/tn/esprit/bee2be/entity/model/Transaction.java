package tn.esprit.bee2be.entity.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import tn.esprit.bee2be.entity.enumeration.TRANSACTION_MODE;
import tn.esprit.bee2be.entity.enumeration.TRANSACTION_TYPE;
@Entity
@Table(name="transaction")
public class Transaction implements Serializable{
	
	private static final long serialVersionUID = 6191889143079517027L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private long amount;
	private String currency;
	private String sender;
	private String Recipient;
	@Temporal(TemporalType.DATE)
	private Date  date;
	@Enumerated(EnumType.STRING)
    private TRANSACTION_MODE mode;
	@Enumerated(EnumType.STRING)
    private TRANSACTION_TYPE type;
	@ManyToOne
	private Account account;
	
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getAmount() {
		return amount;
	}
	public void setAmount(long amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getRecipient() {
		return Recipient;
	}
	public void setRecipient(String recipient) {
		Recipient = recipient;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public TRANSACTION_MODE getMode() {
		return mode;
	}
	public void setMode(TRANSACTION_MODE mode) {
		this.mode = mode;
	}
	public TRANSACTION_TYPE getType() {
		return type;
	}
	public void setType(TRANSACTION_TYPE type) {
		this.type = type;
	}
	public Transaction(long id, long amount, String currency, String sender, String recipient, Date date,
			TRANSACTION_MODE mode, TRANSACTION_TYPE type) {
		super();
		this.id = id;
		this.amount = amount;
		this.currency = currency;
		this.sender = sender;
		Recipient = recipient;
		this.date = date;
		this.mode = mode;
		this.type = type;
	}
	public Transaction( long amount, String currency, String sender, String recipient, Date date,
			TRANSACTION_MODE mode, TRANSACTION_TYPE type) {
		super();

		this.amount = amount;
		this.currency = currency;
		this.sender = sender;
		Recipient = recipient;
		this.date = date;
		this.mode = mode;
		this.type = type;
	}
	public Transaction() {
	}

}
