package tn.esprit.bee2be.entity.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Corporation {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    private String name;

    private String logo;

    private String matriculeFiscale;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date creationDate;

    @OneToOne
    private User user;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date insertedAt;

    public Corporation() {
        super();
    }

    public Corporation(int id, String name, String logo, String matriculeFiscale, Date creationDate) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.matriculeFiscale = matriculeFiscale;
        this.creationDate = creationDate;
    }

    
}
