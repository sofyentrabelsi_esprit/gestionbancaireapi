package tn.esprit.bee2be.entity.model;

import tn.esprit.bee2be.entity.enumeration.MembershipOwner;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Membership")
public class Membership  implements Serializable {

    public Membership(MembershipOwner membershipOwner, List<Products> products, String label) {
        this.membershipOwner = membershipOwner;
        this.products = products;
        this.label = label;
    }

    private static final long serialVersionUID = -47849766742938232L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Enumerated(EnumType.STRING)
    private MembershipOwner membershipOwner;

    @OneToMany
    private List<Products> products;

    private String label;

    public Membership() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }

    public MembershipOwner getMembershipOwner() {
        return membershipOwner;
    }

    public void setMembershipOwner(MembershipOwner membershipOwner) {
        this.membershipOwner = membershipOwner;
    }

    public List<Products> getProducts() {
        return products;
    }

    public void setProducts(List<Products> products) {
        this.products = products;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
