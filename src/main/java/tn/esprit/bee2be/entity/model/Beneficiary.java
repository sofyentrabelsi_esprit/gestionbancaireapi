package tn.esprit.bee2be.entity.model;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "Beneficiary")
public class Beneficiary implements Serializable {

    private static final long serialVersionUID = 5691289108514901358L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;
    private String rib;
    private String adress;

    @CreatedDate
    @Temporal(TemporalType.DATE)
    @Column(name = "createdAt")
    private Date createdAt;

    public Beneficiary(String firstName, String lastName, String rib, String adress, Date createdAt) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.rib = rib;
        this.adress = adress;
        this.createdAt = createdAt;
    }

    public Beneficiary() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
