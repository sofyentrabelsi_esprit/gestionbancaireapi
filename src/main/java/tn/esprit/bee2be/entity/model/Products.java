package tn.esprit.bee2be.entity.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="Products")

public class Products implements Serializable{
	
	private static final long serialVersionUID = 6191889143079517027L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String name;
	private String description;
	private String status;
	private double number;
	

	@OneToOne(mappedBy="products",cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	private Checkbook checkbook ;
	@OneToOne(mappedBy="products",cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	private Account account ;
	@OneToOne(mappedBy="products",cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	private Loan loan;
	@OneToOne(mappedBy="products",cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	private Card card ;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public double getNumber() {
		return number;
	}
	public void setNumber(double number) {
		this.number = number;
	}
	
	public Products(long id, String name, String description, String status, double number) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.status = status;
		this.number = number;
	}
	public Products() {
	
	}
	




}
