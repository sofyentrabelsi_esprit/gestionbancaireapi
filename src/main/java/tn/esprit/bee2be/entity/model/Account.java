package tn.esprit.bee2be.entity.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;

import tn.esprit.bee2be.entity.enumeration.ACCOUNT_TYPE;
@Entity
@Table(name="Account")
public class Account implements Serializable{
	private static final long serialVersionUID = 6191889143079517027L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private double balance;
	private int creditScore;
	@CreatedDate
	@Temporal(TemporalType.DATE)
	@Column(name = "expirationDate")
	private Date expirationDate;
	private String holder;
	@CreatedDate
	@Temporal(TemporalType.DATE)
	@Column(name = "initialDate")
	private Date  initialDate;
	private long number;
	@Enumerated(EnumType.STRING)
	private ACCOUNT_TYPE typeAccount;

	@OneToOne
	private Products products;
	@JsonIgnore
	@OneToMany(mappedBy="account", cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	private List<Transaction> transaction = new ArrayList<>();
	

	public Products getProducts() {
		return products;
	}
	public void setProducts(Products products) {
		this.products = products;
	}
	public List<Transaction> getTransaction() {
		return transaction;
	}
	public void setTransaction(List<Transaction> transaction) {
		this.transaction = transaction;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public ACCOUNT_TYPE getTypeAccount() {
		return typeAccount;
	}
	public void setTypeAccount(ACCOUNT_TYPE typeAccount) {
		this.typeAccount = typeAccount;
	}

	public long getNumber() {
		return number;
	}
	public void setNumber(long number) {
		this.number = number;
	}
	public String getHolder() {
		return holder;
	}
	public void setHolder(String holder) {
		this.holder = holder;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public int getCreditScore() {
		return creditScore;
	}
	public void setCreditScore(int creditScore) {
		this.creditScore = creditScore;
	}
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	public Date getInitialDate() {
		return initialDate;
	}
	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}



	public Account(long id, double balance, int creditScore, Date expirationDate, String holder, Date initialDate,
			long number, ACCOUNT_TYPE typeAccount, Products products, List<Transaction> transaction) {
		super();
		this.id = id;
		this.balance = balance;
		this.creditScore = creditScore;
		this.expirationDate = expirationDate;
		this.holder = holder;
		this.initialDate = initialDate;
		this.number = number;
		this.typeAccount = typeAccount;
		this.products = products;
		this.transaction = transaction;
	}
	public Account() {
	}

}
