package tn.esprit.bee2be.entity.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
 
import tn.esprit.bee2be.entity.enumeration.VehicleType;

@Entity
public class LoanSubCategorie implements Serializable {
	private static final long serialVersionUID = -5369734855993305723L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String  FinancementType; //selon le categorie une liste qui se change
	
	private String TaxPower; //s'affiche selon categorie type 4cv 5cv enum
 
	private VehicleType VehicleType; //s'affiche selon categorie enum 
	
	private String Plafond; 
	
	private int DureeMax;
	
	private int ApportPropre; // si le type est un voiture l'appore propore exige sera calculé
	
	private int MinApportPropre;
	
	
	
	public LoanSubCategorie() {
		super();
		// TODO Auto-generated constructor stub
	}



	public LoanSubCategorie(int id, String financementType, String taxPower,
			tn.esprit.bee2be.entity.enumeration.VehicleType vehicleType, String plafond, int dureeMax, int apportPropre,
			int minApportPropre) {
		super();
		this.id = id;
		FinancementType = financementType;
		TaxPower = taxPower;
		VehicleType = vehicleType;
		Plafond = plafond;
		DureeMax = dureeMax;
		ApportPropre = apportPropre;
		MinApportPropre = minApportPropre;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getFinancementType() {
		return FinancementType;
	}



	public void setFinancementType(String financementType) {
		FinancementType = financementType;
	}



	public String getTaxPower() {
		return TaxPower;
	}



	public void setTaxPower(String taxPower) {
		TaxPower = taxPower;
	}



	public VehicleType getVehicleType() {
		return VehicleType;
	}



	public void setVehicleType(VehicleType vehicleType) {
		VehicleType = vehicleType;
	}



	public String getPlafond() {
		return Plafond;
	}



	public void setPlafond(String plafond) {
		Plafond = plafond;
	}



	public int getDureeMax() {
		return DureeMax;
	}



	public void setDureeMax(int dureeMax) {
		DureeMax = dureeMax;
	}



	public int getApportPropre() {
		return ApportPropre;
	}



	public void setApportPropre(int apportPropre) {
		ApportPropre = apportPropre;
	}



	public int getMinApportPropre() {
		return MinApportPropre;
	}



	public void setMinApportPropre(int minApportPropre) {
		MinApportPropre = minApportPropre;
	}
	 
	
	
	

}
