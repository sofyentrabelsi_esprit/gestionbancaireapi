package tn.esprit.bee2be.entity.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Entity;
import java.util.Date;

@Entity
public class ChargeClientele extends PhysicalPerson {

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startedAt;

    public ChargeClientele() {
        super();
    }

    public ChargeClientele(int id, String firstName, String lastName, String image, String cin, Date birthDate, Date job) {
        super(id, firstName, lastName, image, cin, birthDate);
        this.startedAt = job;
    }
}
