package tn.esprit.bee2be.entity.enumeration;

public enum TRANSACTION_MODE {
	PAYMENT,TRANSFER,CHECK,FEES,WITHDRAWAL,LOAN
}
